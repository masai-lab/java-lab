package xyz.masaimara.lab.concurrency.basic;


import java.math.BigInteger;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

public class ComposedObject {

    public static void main(String... args) {
        testFutureTask();
    }

    private static void testCountDownLatch() {
        try {
            long duration = timeTasks(100, new Runnable() {
                @Override
                public void run() {
                    System.out.println("One more time: " + System.nanoTime());
                }
            });

            System.out.println("Running time:" + duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Latch CountDownLatch实现闭锁
     *
     * @param nThreads
     * @param task
     * @return
     * @throws InterruptedException
     * @see CountDownLatch
     */
    private static long timeTasks(int nThreads, final Runnable task) throws InterruptedException {
        /**
         * 二元闭锁
         */
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(nThreads);

        System.out.println("start time: " + System.nanoTime());
        for (int i = 0; i < nThreads; i++) {
            Thread thread = new Thread() {
                public void run() {
                    try {
                        startGate.await();
                        try {
                            task.run();
                        } finally {
                            endGate.countDown();
                        }
                    } catch (InterruptedException e) {

                    }
                }
            };
            thread.start();
        }

        System.out.println("Running time: " + System.nanoTime());
        long start = System.nanoTime();
        startGate.countDown();
        endGate.await();
        long end = System.nanoTime();
        return end - start;
    }

    /**
     * 测试：使用FutureTask实现可返回的闭锁
     */
    private static void testFutureTask() {
        Preloader preloader = new Preloader();
        preloader.start();
        try {
            System.out.println("Future result: " + preloader.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 实现了Future语义，表示一种抽象的可生成结果的计算。
     * 通过Callable实现，状态：等待、正在运行、执行完成（正常结束、取消而结束和异常结束）。
     * get的行为取决于任务的状态，如果已完成将立即返回结果，否则会阻塞直到任务完成或抛出异常。
     */
    public static class Preloader {
        private final FutureTask<String> future =
                new FutureTask<>(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return "aaa";
                    }
                });
        private final Thread thread = new Thread(future);

        public void start() {
            thread.start();
        }

        public String get() throws Exception {
            try {
                return future.get();
            } catch (ExecutionException e) {
                throw e;
            }
        }
    }

    /**
     * 信号量：Semaphore
     *
     * @param <T>
     */
    private class BoundedHashSet<T> {
        private final Set<T> set;
        private final Semaphore sem;

        public BoundedHashSet(int bound) {
            this.set = Collections.synchronizedSet(new HashSet<>());
            this.sem = new Semaphore(bound);
        }

        public boolean add(T t) throws InterruptedException {
            sem.acquire();
            boolean wasAdded = false;
            try {
                wasAdded = set.add(t);
                return wasAdded;
            } finally {
                if (!wasAdded) {
                    sem.release();
                }
            }
        }

        public boolean remove(Object o) {
            boolean wasRemoved = set.remove(o);
            if (wasRemoved) {
                sem.release();
                return wasRemoved;
            }

            return false;
        }
    }

    public class CellularAutomata {
        private final Board mainBoard;
        private final CyclicBarrier barrier;
        private final Worker[] workers;

        public CellularAutomata(Board mainBoard) {
            this.mainBoard = mainBoard;
            int count = Runtime.getRuntime().availableProcessors();
            this.barrier = new CyclicBarrier(count,
                    new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
            this.workers = new Worker[count];
            for (int i = 0; i < count; i++) {
//                workers[i]=new Worker()
            }
        }

        private class Board {

            public boolean hasConverged() {
                return false;
            }
        }

        private class Worker implements Runnable {
            private final Board board;

            public Worker(Board board) {
                this.board = board;
            }

            @Override
            public void run() {
                while (!board.hasConverged()) {
                    try {
                        barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    } catch (BrokenBarrierException ex) {
                        return;
                    }
                }
            }
        }

        public void start() {
            for (int i = 0; i < workers.length; i++) {
                new Thread(workers[i]).start();
            }
        }

    }

    public static class Scaleable {
        public interface Computable<A, V> {
            V compute(A arg) throws InterruptedException;
        }

        public class ExpensiveFunction implements Computable<String, BigInteger> {

            @Override
            public BigInteger compute(String arg) throws InterruptedException {
                return new BigInteger(arg);
            }
        }

        public class Memoizer1<A, V> implements Computable<A, V> {
            private final Map<A, Future<V>> cache = new ConcurrentHashMap<>();
            private final Computable<A, V> computable;

            public Memoizer1(Computable<A, V> computable) {
                this.computable = computable;
            }

            @Override
            public V compute(final A arg) throws InterruptedException {
                Future<V> f = cache.get(arg);
                if (f == null) {
                    Callable<V> evel = new Callable<V>() {
                        @Override
                        public V call() throws Exception {
                            return computable.compute(arg);
                        }
                    };
                    FutureTask<V> ft = new FutureTask<>(evel);
                    f = cache.putIfAbsent(arg, ft);

                    if (f == null) {
                        f = ft;
                        ft.run();
                    }
                }

                try {
                    return f.get();
                } catch (CancellationException ex) {
                    cache.remove(arg, f);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }
    }
}
